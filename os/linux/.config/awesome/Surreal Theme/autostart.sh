#!/bin/bash

setxkbmap -layout "us,us" -variant "altgr-intl,dvorak-intl" -option "grp:alt_shift_toggle"
