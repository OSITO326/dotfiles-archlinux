export JAVA_HOME='/usr/lib/jvm/default'
export GEM_HOME="$HOME/.gem"
export GOPATH="$HOME/.go"

export FZF_DEFAULT_OPTS='
  --color=pointer:#ebdbb2,bg+:#3c3836,fg:#ebdbb2,fg+:#fbf1c7,hl:#8ec07c,info:#928374,header:#fb4934
  --reverse
'

export path=(
  "$HOME/bin"
  "$DOTLY_PATH/bin"
  "$DOTFILES_PATH/bin"
  "$JAVA_HOME/bin"
  "$GEM_HOME/bin"
  "$GOPATH/bin"
  "$HOME/.cargo/bin"
  "/usr/local/opt/ruby/bin"
  "/usr/local/opt/python/libexec/bin"
  "/usr/local/bin"
  "/usr/local/sbin"
  "/bin"
  "/usr/bin"
  "/usr/sbin"
  "/sbin"
  "$HOME/.npm-global/bin"
)

#Calcurse editor and visual NVIM
export CALCURSE_EDITOR="nvim"
export VISUAL="nvim"

#Qt5 Apps Enviroment
#export QT_QPA_PLATFORMTHEME="qt5ct"

#NPM-Global
#export PATH="$HOME/.npm-global/bin"

