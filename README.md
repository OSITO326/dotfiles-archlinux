<h1 align="center">
  .dotfiles created using <a href="https://github.com/CodelyTV/dotly">🌚 dotly: An opinionated Zsh framework focused on dotfiles 🐢  💨 </a>
</h1>

## Restore your Dotfiles

* Install git
* Clone your dotfiles repository `git clone [your repository of dotfiles] $HOME/.dotfiles`
* Go to your dotfiles folder `cd $HOME/.dotfiles`
* Install git submodules `git submodule update --init --recursive`
* Install your dotfiles, direct on terminal:
  * `export DOTFILES_PATH="$HOME/.dotfiles"`
  * `export DOTLY_PATH="$DOTFILES_PATH/modules/dotly"`
  * `"$DOTLY_PATH/bin/dot" self install`
* Restart your terminal
* Import your packages `dot package import

## Self install
dot self install

## Self update
dot self update

## Update dotly
* cd "$DOTLY_PATH"
* git reset --hard
* git pull origin master
* dot self install

## How to change theme dotly
* To change the theme you must change fpath in .zshrc:
original line `fpath=("$DOTLY_PATH/shell/zsh/themes" "$DOTLY_PATH/shell/zsh/completions" $fpath)` to `export fpath=("$HOME/.dotfiles/shell/zsh/themes" "$DOTLY_PATH/shell/zsh/completions" $fpath)`.
* At this moment `prompt` just load theme dotly. for that you must change to access your dotfiles/
* You can you see others themes for default with this command: `prompt -l`, like `prompt bart`.
* If you want create a new one theme, remember following the convention: `prompt_name_setup`.
* For add new of the themes of ZSH, [click here to see all the themes on ZSH repo](https://github.com/ohmyzsh/ohmyzsh/wiki/Themes)
* To create a the theme just copy the line into the new file, (always remember the convention)-> prompt_name_setup copy the content inside in your new file, example `prompt_nanotech_setup` inside this. To see the configuration that you choose,[click here to see the configuration](https://github.com/ohmyzsh/ohmyzsh/tree/master/themes) copy the `nanotech.zsh-theme` into `prompt_nanotech_setup`. 

![Change theme dotly](.screenshots/change_theme_dotly.png)

## Original:
https://github.com/CodelyTV/dotly

